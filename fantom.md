# BuildingFit Fantom Style Guide

## 1. Introduction
Like other programming style guides, the issues covered span not only aesthetic issues of formatting, but other types of conventions or
coding standards as well. However, this document focuses primarily on the **hard-and-fast rules** that we follow universally, and avoids
giving advice that isn't clearly enforceable (whether by human or tool).

### 1.1 Terminology notes
In this document, unless otherwise clarified:

- Class: used inclusively to mean an "ordinary" class, enum class, interface or annotation type (`@interface`).
- Member (of a class): used inclusively to mean a nested class, field, method, or constructor; that is, all top-level contents of a class
except initializers and comments.
- Comment: always refers to *implementation* comments. We do not use the phrase "documentation comments", instead using the term
"fandoc".

Other "terminology notes" will appear occasionally throughout the document.

### 1.2 Contributors
This document was created by Justin Sampson and is maintained by [Jay Herron](https://www.linkedin.com/in/jay-herron-4ab86972/).
It is heavily based on the excellent [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).

## 2. Source files

### 2.1 Pod Structure and naming
All BuildingFit pods are named following the structure: `bfit<name>`. If the pod functions as a SkySpark extension by implementing the
`skyarcd::Ext` class then the pod file name should additionally end with `Ext`.

The following structure must be maintained within the pod:

- Root directories:
    - `fan`: Directory which houses non-test Fantom classes. Fantom functionality is conceptually organized within subdirectories, such as
    `ui`, etc.
    - `test`: Directory which houses test Fantom classes
    - `lib`: Directory which houses trio files containing func, tag, def, view, app, or template records that should be automatically
    inserted into the Axon namespace. Subdirectories can be created as necessary to organize files.
        - For func files, follow the one-record-per-file convention. Tags, views, apps, and templates may have multiple records per file.
    - `res`: Directory which houses additional resources files. Ex: images, csvs, etc.
- Root files:
    - `build.fan`: Pod file definition.
    - `releaseNotes.fandoc`: This file is used to track changes made at each version update.
    - `pod.fandoc`: General pod documentation. See Fandoc section.
    - `README.md`: Internal documentation detailing any information necessary for building the pod, setup, and/or other necessary notes.

### 2.2  File names

#### 2.2.1 Fantom
The source file name consists of the case-sensitive name of the class it contains, plus the `.fan extension. If the
file contains multiple classes (one public and multiple internal classes), then the name should match the public class name.

#### 2.2.2 Axon
Axon files within a pod must be housed within a subdirectory that is given a short name indicating the type of functionality. All axon
files within the subdirectory should be prefixed with the subdirectory name. For example, all axon KPIs would be housed under the `kpi`
subdirectory. Further subdirectories can be included to increase specificity. Given a KPI which runs on an AHU. The file name would begin
with `kpiAhu`.

### 2.3 File encoding: UTF-8
Source files are encoded in UTF-8.

### 2.4 Special characters

#### 2.4.1 Whitespace characters
Aside from the line terminator sequence, the ASCII horizontal space character (`0x20`) is the only whitespace character that appears
anywhere in a source file. This implies that:

1. All other whitespace characters in string and character literals are escaped.
2. Tab characters are not used for indentation.

All tabs should instead be replaced with a sequence of two spaces.

#### 2.4.2 Special escape sequences
For any character that has a special escape sequence (`\b`, `\t`, `\n`, `\f`, `\r`, `\"`, `\'` and `\\`), that sequence is used rather than
the corresponding Unicode (e.g. `\u000a`) escape.

#### 2.4.3 Non-ASCII characters
For the remaining non-ASCII characters, either the actual Unicode character (e.g. `∞`) or the equivalent Unicode escape (e.g. `\u221e`) is
used. The choice depends only on which makes the code easier to read and understand, although Unicode escapes outside string literals and
comments are strongly discouraged. Examples:

| Example | Discussion |
|---------|------------|
| `Str unitAbbrev := "μs"`` | Best: perfectly clear even without a comment. |
| `Str unitAbbrev := "\u03bcs" // Greek letter mu, "s"` | Allowed, but awkward and prone to mistakes. |
| `Str unitAbbrev := "\u03bcs"` | Poor: the reader has no idea what this is. |
| `Str body := '\ufeff' + content // byte order mark` | Good: use escapes for non-printable characters, and comment if necessary. |

**Tip**: Never make your code less readable simply out of fear that some programs might not handle non-ASCII characters properly. If that
should happen, those programs are **broken** and they must be **fixed**.

## 3 Source file structure
A source file consists of, **in order**:

1. Documentation that explains what the class/file contains and its purpose
2. Using statements
3. One or zero public classes
4. Zero or more internal classes

**Exactly one blank line** separates each section that is present.

### 3.1 Using statements
Using statements are ordered alphabetically, each on a single line, with no blank lines between them. However, if there are both Fantom and
Java interop using statements, they are broken into two alphabetized blocks with a single blank line between them.

### 3.2 Class declaration

#### 3.2.1 Public class declaration
Each file contains only one or zero public classes.

If a public class **is** present in the file, a file may include as many internal classes as desired, with the restriction that internal
classes in the file should be closely related to the public class (helper classes).

If a public class **is not** present in the file, then the file should contain only a single internal class. That is, files with no public
class, but multiple internal classes are not allowed.

#### 3.2.2 Ordering of class contents
The order you choose for the members and initializers of your class can have a great effect on learnability. However, there's no single
correct recipe for how to do it; different classes may order their contents in different ways.

What is important is that each class uses *some* logical order, which its maintainer could explain if asked. For example, new methods are
not just habitually added to the end of the class, as that would yield "chronological by date added" ordering, which is not a logical
ordering.

## 4 Formatting
**Terminology Note**: *block-like construct* refers to the body of a class, method or constructor. Note that, by Section 4.8.3.1 on array
initializers, any array initializer *may* optionally be treated as if it were a block-like construct.

### 4.1 Braces

#### 4.1.1 Braces are used where optional
Braces are used with if, else, for, do and while statements, even when the body is empty or contains only a single statement.

    // Acceptable
    if (condition()) {
      something()
    }

    // Not acceptable
    if (condition()) something()

    // Not acceptable
    if (condition())
      something()

#### 4.1.2 Nonempty blocks: K & R style
Braces follow the Kernighan and Ritchie style (also called "[Egyptian brackets](https://blog.codinghorror.com/new-programming-jargon/)")
for nonempty blocks and block-like constructs:

- No line break before the opening brace.
- Line break after the opening brace.
- Line break before the closing brace.
- Line break after the closing brace, only if that brace terminates a statement or terminates the body of a method, constructor, or named
class. For example, there is no line break after the brace if it is followed by else, comma, or closing parenthesis.

Examples:

    public Void method() {
      while (condition()) {
        otherMethod()
      }
    }

    MyClass {
      @Override public Void method() {
        if (condition()) {
          try {
            something()
          } catch (ProblemException e) {
            recover()
          }
        } else if (otherCondition()) {
          somethingElse()
        } else {
          lastThing()
        }
      }
    }

A few exceptions for enum classes are given in Section 4.8.1, Enum classes.

#### 4.1.3 Empty blocks: may be concise
An empty block or block-like construct may be in K & R style (as described in Section 4.1.2). Alternatively, it may be closed immediately
after it is opened, with no characters or line break in between (`{}`), unless it is part of a multi-block statement (one that directly
contains multiple blocks: if/else or try/catch/finally). Examples:

    // Acceptable
    void doNothing() {}

    // Also acceptable
    void doNothingElse() {
    }

    // NOT acceptable: No concise empty blocks in a multi-block statement
    try {
      doSomething();
    } catch (Exception e) {}

### 4.2 Block indentation: +2 spaces
Each time a new block or block-like construct is opened, the indent increases by two spaces. When the block ends, the indent returns to the
previous indent level. The indent level applies to both code and comments throughout the block. (See the example in Section 4.1.2, Nonempty
  blocks: K & R Style.)

### 4.3 One statement per line
Each statement is followed by a line break.

### 4.4 Column limit: 140
The line length should be limited to a maximum of 140 characters. A "character" means any Unicode code point. Except as noted below, any
line that would exceed this limit must be line-wrapped, as explained in the Line-Wrapping section.

Each Unicode code point counts as one character, even if its display width is greater or less. For example, if using fullwidth characters,
you may choose to wrap the line earlier than where this rule strictly requires.

Exceptions:

1. Lines where obeying the column limit is not possible (for example, a long URL in fandoc).
2. using statements (see 3.2 Using statements).
3. Command lines in a comment that may be cut-and-pasted into a shell.

### 4.5 Line-wrapping
**Terminology Note**: When code that might otherwise legally occupy a single line is divided into multiple lines, this activity is called
*line-wrapping*.

There is no comprehensive, deterministic formula showing exactly how to line-wrap in every situation. Very often there are several valid
ways to line-wrap the same piece of code. While the typical reason for line-wrapping is to avoid overflowing the column limit, even code
that would in fact fit within the column limit may be line-wrapped at the author's discretion.

**Tip**: Extracting a method or local variable may solve the problem without the need to line-wrap.

#### 4.5.1 Where to break lines
The prime directive of line-wrapping is: prefer to break at a **higher syntactic level**. Also:

1. When a line is broken at a non-assignment operator the break comes before the symbol.
    - This also applies to the following "operator-like" symbols:
        - the dot separator (.)
        - the two colons of a method reference (::)
2. When a line is broken at an *assignment* operator the break typically comes *after* the symbol, but either way is acceptable.
3. A method or constructor name stays attached to the open parenthesis (() that follows it.
4. A comma (,) stays attached to the token that precedes it.

**Note**: The primary goal for line wrapping is to have clear code, *not necessarily* code that fits in the smallest number of lines.

#### 4.5.2 Indent continuation lines at least +4 spaces
When line-wrapping, each line after the first (each *continuation line*) is indented at least +4 from the original line.

When there are multiple continuation lines, indentation may be varied beyond +4 as desired. In general, two continuation lines use the same
indentation level if and only if they begin with syntactically parallel elements.

The Horizontal Alignment section addresses the discouraged practice of using a variable number of spaces to align certain tokens with
previous lines.

### 4.6 Whitespace

#### 4.6.1 Vertical whitespace
A single blank line always appears:

1. Between consecutive members or initializers of a class: fields, constructors, methods, nested classes, static initializers, and instance
initializers.
    - **Exception**: A blank line between two consecutive fields (having no other code between them) is optional. Such blank lines are used
    as needed to create logical groupings of fields.
    - **Exception**: Blank lines between enum constants are covered in Section 4.8.1.
2. As required by other sections of this document (such as Section 3, Source file structure, and Section 3.2, using statements).

A single blank line may also appear anywhere it improves readability, for example between statements to organize the code into logical
subsections. A blank line before the first member or initializer, or after the last member or initializer of the class, is neither
encouraged nor discouraged.

Multiple consecutive blank lines are permitted, but never required (or encouraged).

#### 4.6.2 Horizontal whitespace
Beyond where required by the language or other style rules, and apart from literals, comments and fandoc, a single ASCII space also appears
in the following places **only**.

- Separating any reserved word, such as if, for or catch, from an open parenthesis (`(`) that follows it on that line
- Separating any reserved word, such as else or catch, from a closing curly brace (`}`) that precedes it on that line
- Before any open curly brace `{`. Exceptions:
    - `@SomeAnnotation({a, b})` (no space is used)
- On both sides of any binary or ternary operator. Exceptions:
    - The two colons (`::`) of a method reference, which is written like `Object::toString`
    - The dot separator (`.`), which is written like `object.toString()`
    - The get operator `[]`, which is written like `list[2]` or `map["name"]`
    - The trap operator `->`, which is written like `map->name`
    - The range operator `..`, which is written like `list[0..5]`
- After `,:;` or the closing parenthesis (`)`) of a cast
- On both sides of the double slash (`//`) that begins an end-of-line comment. Here, multiple spaces are allowed, but not required.
- Between the type and variable of a declaration: `Str[] list`
- Just inside both brackets of a list or map initializer (optional): `Int[] a := [ 1, 2, 3 ]` and `Int[] a := [1, 2, 3]` are both valid.

#### 4.6.3 Horizontal alignment: never required
Horizontal alignment is the practice of adding a variable number of additional spaces in your code with the goal of making certain tokens
appear directly below certain other tokens on previous lines. This practice is permitted, but is never required. It is not even required
to maintain horizontal alignment in places where it was already used.

Here is an example without alignment, then using alignment:

    private Int x // this is fine
    private Color color // this too

    private Int   x      // permitted, but future edits
    private Color color  // may leave it unaligned

**Tip**: Alignment can aid readability, but it creates problems for future maintenance. Consider a future change that needs to touch just
one line. This change may leave the formerly-pleasing formatting mangled, and that is **allowed**. More often it prompts the coder (perhaps
you) to adjust whitespace on nearby lines as well, possibly triggering a cascading series of reformattings. That one-line change now has
a "blast radius." This can at worst result in pointless busywork, but at best it still corrupts version history information, slows down
reviewers and exacerbates merge conflicts.

### 4.7 Grouping parentheses: recommended
**Optional grouping parentheses are omitted only when author and reviewer agree that there is no reasonable chance the code will be
misinterpreted without them**, nor would they have made the code easier to read. It is *not* reasonable to assume that every reader has the
entire Fantom operator precedence table memorized.

### 4.8 Specific constructs

#### 4.8.1 Variable declarations

##### 4.8.1.1 One variable per declaration
Every variable declaration (field or local) declares only one variable.

**Exception**: Multiple variable declarations are acceptable in the header of a for loop.

##### 4.8.1.2 Type is explicit
All variables should explicitly state their type at creation. The nullable indicator should NOT be used in cases where null is not
expected to be stored in a variable. Example:

    Str text := “text” // Acceptable
    text := “text”    // NOT acceptable

##### 4.8.1.3 Declared when needed
Local variables are not habitually declared at the start of their containing block or block-like construct. Instead, local variables are
declared close to the point they are first used (within reason), to minimize their scope. Local variable declarations typically have
initializers, or are initialized immediately after declaration.

#### 4.8.2 Enum classes
After each comma that follows an enum constant, a line break is optional. Additional blank lines (usually just one) are also allowed. This
is one possibility:

    enum class Answer {
      YES,
      NO,
      MAYBE
    }

An enum class with no methods and no documentation on its constants may optionally be formatted as if it were an array initializer
(see Arrays section).

    enum class Suit { CLUBS, HEARTS, SPADES, DIAMONDS }

Since enum classes *are classes*, all other rules for formatting classes apply.

#### 4.8.3 Collections

##### 4.8.3.1 Lists
Any list initializer may *optionally* be formatted as if it were a "block-like construct." For example, the following are all valid (not
an exhaustive list):

    Int[0, 1, 2, 3]

    Int[
      0, 1, 2, 3
    ]

    Int[
      0, 1,
      2, 3
    ]

    Int[
      0,
      1,
      2,
      3,
    ]             

If declaring multiple items on the same line, there should be one space between a `,` and the next item.

##### 4.8.3.1 Maps
Any map initializer may *optionally* be formatted as if it were a "block-like construct." For example, the following are all valid (not
an exhaustive list):

    Int["a": 0, "b": 1, "c": 2, "d": 3]

    Int[
      "a": 0, "b": 1, "c": 2, "d": 3
    ]

    Int[
      "a": 0, "b": 1,
      "c": 2, "d": 3
    ]

    Int[
      "a": 0,
      "b": 1,
      "c": 2,
      "d": 3
    ]

There should be one space following any `:` between an item’s name its value. Similar to lists, if declaring multiple items on the same
line, there should be one space between a , and the next item.

#### 4.8.4 Switch statements
**Terminology Note**: Inside the braces of a *switch block* are one or more *statement groups*. Each statement group consists of one or more
*switch labels* (either `case FOO:` or `default:`), followed by one or more statements (or, for the last statement group, zero or more
statements).

##### 4.8.4.1 Indentation
As with any other block, the contents of a switch block are indented +2.

After a switch label, there is a line break, and the indentation level is increased +2, exactly as if a block were being opened. The
following switch label returns to the previous indentation level, as if a block had been closed.

    switch (input) {
      case 1:
      case 2:
        handleOneOrTwo()
        // Fantom doesn't support fall through
      case 3:
        handleThree()
      default:
        handleLargeNumber(input)
    }

##### 4.8.4.2 The `default` case is present
Each switch statement includes a default statement group, even if it contains no code.

**Exception**: A switch statement for an enum type may omit the default statement group, if it includes explicit cases covering all
possible values of that type. This enables IDEs or other static analysis tools to issue a warning if any cases were missed.

#### 4.8.5 Facets
Facets applying to a class, method or constructor appear immediately after the documentation block, and each annotation is listed on a
line of its own (that is, one annotation per line). These line breaks do not constitute line-wrapping (Section 4.5, Line-wrapping), so the
indentation level is not increased. Example:

    @Axon
    @NoDoc
    public String getNameIfPresent() { ... }

There are no specific rules for formatting annotations on parameters, local variables, or types.

#### 4.8.6 Comments
This section addresses *implementation comments*.

Any line break may be preceded by arbitrary whitespace followed by an implementation comment. Such a comment renders the line non-blank.

##### 4.8.6.1 Block comment style
Block comments are indented at the same level as the surrounding code. They may be in `/* ... */` style or `// ...` style. For multi-line
`/* ... */` comments, subsequent lines must start with `*` aligned with the `*` on the previous line.

    /*
     * This is
     * okay.
     */

    // And so
    // is this.

    /* Or you can
     * even do this */

Comments are not enclosed in boxes drawn with asterisks or other characters.

#### 4.8.7 Lambda functions
Lambda function statements should use the following structure even when shortcuts are possible. This includes parenthesis after the lambda
function call and explicit type declaration:

    gridVariable.each(| Type x | {
      // do stuff
    })

    gridVariable.map(| Type x -> Bool | {
      // do stuff
    })

#### 4.8.8 Method calls are explicit
Even if no parameters are passed to a method, parentheses are still included:

    val.toStr() // Acceptable
    val.toStr // Not acceptable

## 5 Naming

### 5.1 Rules common to all identifiers
Identifiers use only ASCII letters and digits, and, in a small number of cases noted below, underscores. Thus each valid identifier name is
matched by the regular expression `\w+`.

Special prefixes or suffixes are not used. For example: `name_`, `mName`, `s_name` and `kName`.

### 5.2 Rules by identifier type

#### 5.2.1 Class names
Class names are written in **UpperCamelCase**.

Class names are typically nouns or noun phrases. For example, `Character` or `ImmutableList`. Interface names may also be nouns or noun
phrases (for example, `List`), but may sometimes be adjectives or adjective phrases instead (for example, `Readable`).

There are no specific rules or even well-established conventions for naming annotation types.

Test classes are named starting with the name of the class they are testing, and ending with Test. For example, HashTest or
HashIntegrationTest.

#### 5.2.2 Method names
Method names are written in **lowerCamelCase**.

Method names are typically verbs or verb phrases. For example, `sendMessage` or `stop`.

Underscores may appear in test method names [JH30] to separate logical components of the name, with *each* component written in
lowerCamelCase. One typical pattern is `<methodUnderTest>_<state>`, for example `pop_emptyStack`. There is no One Correct Way to name test
methods.

#### 5.2.3 Constant names
Constant names use **CONSTANT_CASE**: all uppercase letters, with each word separated from the next by a single underscore. But what is a
constant, exactly?

Constants are static final fields whose contents are deeply immutable and whose methods have no detectable side effects. This includes
primitives, Strings, immutable types, and immutable collections of immutable types. If any of the instance's observable state can change,
it is not a constant. Merely intending to never mutate the object is not enough.

These names are typically nouns or noun phrases.

#### 5.2.4 Non-constant field names
Non-constant field names (static or otherwise) are written in **lowerCamelCase**. These names are typically nouns or noun phrases. For
example, computedValues or index.

#### 5.2.5 Parameter names
Parameter names are written in **lowerCamelCase**. One-character parameter names in public methods should be avoided.

#### 5.2.6 Local variable names
Local variable names are written in **lowerCamelCase**. Even when final and immutable, local variables are not considered to be constants,
and should not be styled as constants.

### 5.3 Camel case defined
Sometimes there is more than one reasonable way to convert an English phrase into camel case, such as when acronyms or unusual constructs
like "IPv6" or "iOS" are present. To improve predictability, we specify the following (nearly) deterministic scheme.

Beginning with the prose form of the name:

1. Convert the phrase to plain ASCII and remove any apostrophes. For example, "Müller's algorithm" might become "Muellers algorithm".
2. Divide this result into words, splitting on spaces and any remaining punctuation (typically hyphens).
    - *Recommended*: if any word already has a conventional camel-case appearance in common usage, split this into its constituent parts
    (e.g., "AdWords" becomes "ad words"). Note that a word such as "iOS" is not really in camel case per se; it defies any convention, so
    this recommendation does not apply.
3. Now lowercase everything (including acronyms), then uppercase only the first character of:
    - ... each word, to yield upper camel case, or
    - ... each word except the first, to yield lower camel case
4. Finally, join all the words into a single identifier.

Note that the casing of the original words is almost entirely disregarded. Examples:

| Prose form | Correct | Incorrect |
|------------|---------|-----------|
| "XML HTTP request" | XmlHttpRequest | XMLHTTPRequest |
| "new customer ID" | newCustomerId | newCustomerID |
| "inner stopwatch" | innerStopwatch | innerStopWatch |
| "supports IPv6 on iOS?" | supportsIpv6OnIos | supportsIPv6OnIOS |
| "YouTube importer" | YouTubeImporter/YoutubeImporter* | |

\*Acceptable, but not recommended.

**Note**: Some words are ambiguously hyphenated in the English language: for example "nonempty" and "non-empty" are both correct, so the
method names `checkNonempty` and `checkNonEmpty` are likewise both correct.

## 6 Programming Practices

### 6.1 Caught exceptions are not ignored
It is very rarely correct to do nothing in response to a caught exception. (Typical responses are to log it, or if it is considered
"impossible", rethrow it.)

When it truly is appropriate to take no action whatsoever in a catch block, the reason should be explained in a comment.

    try {
      Int i := Int.parseInt(response)
      return handleNumericResponse(i)
    } catch (NumberFormatException ok) {
      // it's not numeric; that's fine, just continue
    }
    return handleTextResponse(response)

### 6.2 Static members are qualified using class
When a reference to a static class member must be qualified, it is qualified with that class's name, not with a reference or expression of
that class's type.

    Foo aFoo = ...
    Foo.aStaticMethod() // good
    aFoo.aStaticMethod() // bad
    somethingThatYieldsAFoo().aStaticMethod() // very bad

## 7. Fandoc

### 7.1 Pod Doc
A pod.fandoc file must be present. The first section should be an Overview describing the overarching purpose of the pod. Following this
should be sections explaining configuration, usage, included views, etc.

### 7.2 Func Doc
Any Axon-accessible method (Fantom or Axon) must have documentation following the following format:

    Explanation of functionality. First sentence is most important, as it’s included
    in the total list of functions.

    **Param**

    - 'param1': explanation of param details

    **Return**

    Explanation of what is returned. This may be excluded if nothing is returned.
