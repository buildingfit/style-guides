# BuildingFit Axon Style Guide

## 1. Introduction
Axon is a programming language used in the [SkySpark](https://skyfoundry.com/) fault-detection platform. While axon snippits can be used
in many areas of the SkySpark platform (jobs, for example), this style guide focuses on Axon code that is contained in the `src` tag
of a `func` record.

Like other programming style guides, the issues covered span not only aesthetic issues of formatting, but other types of conventions or
coding standards as well. However, this document focuses primarily on the **hard-and-fast rules** that we follow universally, and avoids
giving advice that isn't clearly enforceable (whether by human or tool).

### 1.1 Terminology notes
In this document, unless otherwise clarified:

- Comment: always refers to *implementation* comments. We do not use the phrase "documentation comments", instead using the term
"fandoc".

Other "terminology notes" will appear occasionally throughout the document.

### 1.2 Contributors
This document was created and is maintained by [Jay Herron](https://www.linkedin.com/in/jay-herron-4ab86972/). It is heavily based on the
excellent [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).

## 2. Source files
This section only pertains to Axon funcs contained in a Fantom pod. See the Fantom Style guide for organization and formatting details.

### 2.1 File format
Axon functions are stored in files formatted as [trio](https://project-haystack.org/doc/Trio) to facilitate automatic importing
into the SkySpark namespace.

### 2.2  File names and organization
To support pods with a varying number of funcs, two distinct configurations are allowed:

1. All func records are contained in a single trio file named `funcs.trio`. This file must be placed at the root of the `lib` directory.
This should only be used if there are about 10 or less funcs in the pod.
2. All func records are in separate files, each one named the same as the func `name` tag. This should be used if there are about 10 or
more funcs in the pod. The resulting files can be flat in the `lib` directory or categorized into subdirectories. The convention is to make
the directory name the beginning of the func name. For example, if a function is in the `lib/kpi/ahu` folder, it will start with `kpiAhu`.

### 2.3 File encoding: UTF-8
Source files are encoded in UTF-8.

### 2.4 Special characters

#### 2.4.1 Whitespace characters
Aside from the line terminator sequence, the ASCII horizontal space character (`0x20`) is the only whitespace character that appears anywhere
in a source file. This implies that:

1. All other whitespace characters in string and character literals are escaped.
2. Tab characters are not used for indentation.

All tabs should instead be replaced with a sequence of two spaces.

#### 2.4.2 Special escape sequences
For any character that has a special escape sequence (`\b`, `\t`, `\n`, `\f`, `\r`, `\"`, `\'` and `\\`), that sequence is used rather than
the corresponding Unicode (e.g. `\u000a`) escape.

#### 2.4.3 Non-ASCII characters
For the remaining non-ASCII characters, either the actual Unicode character (e.g. `∞`) or the equivalent Unicode escape (e.g. `\u221e`) is
used. The choice depends only on which makes the code easier to read and understand, although Unicode escapes outside string literals and
comments are strongly discouraged. Examples:

| Example | Discussion |
|---------|------------|
| `unitAbbrev: "μs"` | Best: perfectly clear even without a comment. |
| `unitAbbrev: "\u03bcs" // Greek letter mu, "s"` | Allowed, but awkward and prone to mistakes. |
| `unitAbbrev: "\u03bcs"` | Poor: the reader has no idea what this is. |
| `body:  "\ufeff" + content // byte order mark` | Good: use escapes for non-printable characters, and comment if necessary. |

**Tip**: In the Unicode escape case, and occasionally even when actual Unicode characters are used, an explanatory comment can be very
helpful.

## 3 Source file structure
This section only pertains to Axon funcs contained in a Fantom pod.

### 3.1 Tag order is alphabetical
The trio file tags should be ordered alphabetically, with the following exceptions:

- `name` should be the first tag
- `dis`, if present (typically only on rule-ready funcs), should come immediately after the `name` tag
- `src` should be the last tag, as it may be long, and tags that follow it are more likely to go unnoticed.

### 3.2 Func order is alphabetical
If storing more than 1 function in a single trio file, they should be ordered alphabetically by the `name` tag.

### 3.3 Doc tag is present
A `doc` tag should be present, regardless of whether the func has the `nodoc` tag. See the Fantom 'Func Doc' section for details on format.

## 4 Formatting
This section pertains to Axon funcs stored as a SkySpark record, or contained in a Fantom pod. Specifically, it applies to the contents
of the `src` tag.

### 4.1 Blocks
In Axon, the `do` and `end` keyworks identify the [blocks](https://skyfoundry.com/doc/docSkySpark/AxonLang#blocks)

#### 4.1.1 Do and end are used where optional
The `do` and `end` keywords must be used with `if`, `else`, `try`, `catch`, and lambda statements, even when the body contains only a
single statement, unless specifically exempted elsewhere in this document. The `end` keyword is never omitted when it immediately preceeds
`else` or `catch`.

    // Acceptable
    if (condition()) do
      something()
    end else do
      somethingElse()
    end

    // Not acceptable
    if (condition()) something()
    else somethingElse()

    // Not acceptable
    if (condition())
      something()
    else
      somethingElse()

    // Not acceptable
    if (condition()) do
      something()
    else do  // end is omitted
      somethingElse()
    end

#### 4.1.2 K & R style
Blocks follow the Kernighan and Ritchie style (also called "[Egyptian brackets](https://blog.codinghorror.com/new-programming-jargon/)")
for nonempty blocks and block-like constructs (obviously, with brackets replaced by `do`/`end`):

- No line break before `do`.
- Line break after `do`.
- Line break before `end`.
- Line break after `end` only if it terminates a statement. For example, there is no line break after `end` if it is followed by
`else`, a comma, or a closing parenthesis.

Examples:

    if (condition()) do
      try do
        something()
      end catch (err) do
        recover()
      end
    end else if (otherCondition()) do
      somethingElse()
    end else do
      lastThing()
    end

**Note**: Empty blocks are not supported by Axon.

### 4.2 Block indentation: +2 spaces
Each time a new block or block-like construct is opened, the indent increases by two spaces. When the block ends, the indent returns to the
previous indent level. The indent level applies to both code and comments throughout the block. (See the example in Section 4.1.2, Nonempty
blocks: K & R Style.)

### 4.3 One statement per line
Each statement is followed by a line break.

### 4.4 Column limit: 140
The line length should be limited to a maximum of 140 characters. A "character" means any Unicode code point. Except as noted below, any
line that would exceed this limit must be line-wrapped, as explained in the Line-Wrapping section.

Each Unicode code point counts as one character, even if its display width is greater or less. For example, if using fullwidth characters,
you may choose to wrap the line earlier than where this rule strictly requires.

Exceptions:

1. Lines where obeying the column limit is not possible (for example, a long URL).
2. Command lines in a comment that may be cut-and-pasted into a shell.

### 4.5 Line-wrapping
**Terminology Note**: When code that might otherwise legally occupy a single line is divided into multiple lines, this activity is called
*line-wrapping*.

There is no comprehensive, deterministic formula showing exactly how to line-wrap in every situation. Very often there are several valid
ways to line-wrap the same piece of code. While the typical reason for line-wrapping is to avoid overflowing the column limit, even code
that would in fact fit within the column limit may be line-wrapped at the author's discretion.

**Tip**: Extracting a lambda or variable may solve the problem without the need to line-wrap.

#### 4.5.1 Where to break lines
The prime directive of line-wrapping is: prefer to break at a **higher syntactic level**. Also:

1. When a line is broken at a *non-assignment* operator the break comes before the symbol.
    - This also applies to the following "operator-like" symbols:
        - the dot separator (`.`)
2. When a line is broken at an *assignment* operator the break typically comes *after* the symbol, but either way is acceptable.
3. A func name stays attached to the open parenthesis (`(`) that follows it.
4. A comma (`,`) stays attached to the token that precedes it.

The primary goal for line wrapping is to have clear code, not necessarily code that fits in the smallest number of lines.

#### 4.5.2 Indent continuation lines at least +4 spaces
When line-wrapping, each line after the first (each *continuation line*) is indented at least +4 from the original line.

When there are multiple continuation lines, indentation may be varied beyond +4 as desired. In general, two continuation lines use the same
indentation level if and only if they begin with syntactically parallel elements.

The Horizontal Alignment section addresses the discouraged practice of using a variable number of spaces to align certain tokens with
previous lines.

### 4.6 Whitespace

#### 4.6.1 Vertical whitespace
There are no places where vertical whitespace is required. A single blank line may appear anywhere it improves readability, for example
between statements to organize the code into logical subsections. Multiple consecutive blank lines are permitted, but never required
(or encouraged).

#### 4.6.2 Horizontal whitespace
Beyond where required by the language or other style rules, and apart from literals, comments and fandoc, a single ASCII space also appears
in the following places **only**.

- Separating any reserved word, such as `if` or `catch`, from an open parenthesis (`(`) that follows it on that line
- On both sides of any [operator](https://skyfoundry.com/doc/docSkySpark/AxonLang#operators). Exceptions:
    - The assignment operator (`:`), which is written like `word: "peanut"`
    - The dot separator (`.`), which is written like `object.toStr()`
    - The get operator (`[]`), which is written like `list[2]` or `dict["name"]`
    - The trap operator (`->`), which is written like `map->name`
    - The range operator (`..`), which is written like `list[0..5]`
- After `,` or `:`
- On both sides of the double slash (`//`) that begins an end-of-line comment. Here, multiple spaces are allowed, but not required.
- Just inside both brackets of a list or map initializer (optional): `a: [ 1, 2, 3 ]` and `a: [1, 2, 3]` are both valid.

#### 4.6.3 Horizontal alignment
Horizontal alignment is the practice of adding a variable number of additional spaces in your code with the goal of making certain tokens
appear directly below certain other tokens on previous lines. This practice is permitted, but is never required. It is not even required
to maintain horizontal alignment in places where it was already used.

Here is an example without alignment, then using alignment:

    speed: 5 // this is fine
    position: 10 // this too

    speed:    5  // permitted, but future edits
    position: 10 // may leave it unaligned

**Tip**: Alignment can aid readability, but it creates problems for future maintenance. Consider a future change that needs to touch just
one line. This change may leave the formerly-pleasing formatting mangled, and that is **allowed**. More often it prompts the coder (perhaps
you) to adjust whitespace on nearby lines as well, possibly triggering a cascading series of reformattings. That one-line change now has
a "blast radius." This can at worst result in pointless busywork, but at best it still corrupts version history information, slows down
reviewers and exacerbates merge conflicts.

### 4.7 Grouping parentheses
**Optional grouping parentheses are omitted only when author and reviewer agree that there is no reasonable chance the code will be
misinterpreted without them**, nor would they have made the code easier to read. It is *not* reasonable to assume that every reader has the
entire Axon operator precedence table memorized.

### 4.8 Specific constructs

#### 4.8.1 Variables
Variable assignment has **no space** between the declared name and the assignment operator (`:`). However, variable reassignment **does
have a space** between the declared name and the reassignment operator (`=`). Example:

    word: "peanut"
    word = "fish"

Variables are not habitually declared at the start of their containing block or block-like construct. Instead, they are declared close to
the point they are first used (within reason).

#### 4.8.2 Collections

##### 4.8.2.1 Lists
Any list literal may *optionally* be formatted as if it were a "block-like construct." For example, the following are all valid (not
an exhaustive list):

    [0, 1, 2, 3]

    [
      0, 1, 2, 3
    ]

    [
      0, 1,
      2, 3
    ]

    [
      0,
      1,
      2,
      3,
    ]             

If declaring multiple items on the same line, there should be one space between a `,` and the next item.

##### 4.8.2.2 Dicts
Any dict literal may *optionally* be formatted as if it were a "block-like construct." For example, the following are all valid (not
an exhaustive list):

    {a: 0, b: 1, c: 2, d: 3}

    {
      a: 0, b: 1, c: 2, d: 3
    }

    {
      a: 0, b: 1,
      c: 2, d: 3
    }

    {
      a: 0,
      b: 1,
      c: 2,
      d: 3
    }

There should be one space following any `:` between an item’s name its value. Similar to lists, if declaring multiple items on the same
line, there should be one space between a `,` and the next item. Dict key names are not quoted.

#### 4.8.3 Comments
This section addresses *implementation comments*.

Any line break may be preceded by arbitrary whitespace followed by an implementation comment. Such a comment renders the line non-blank.

##### 4.8.3.1 Block comment style
Block comments are indented at the same level as the surrounding code. They may be in `/* ... */` style or `// ...` style. For multi-line
`/* ... */` comments, subsequent lines must start with `*` aligned with the `*` on the previous line.

    /*
     * This is
     * okay.
     */

    // And so
    // is this.

    /* Or you can
     * even do this */

Comments are not enclosed in boxes drawn with asterisks or other characters.

#### 4.8.4 Lambdas
[Lambdas](https://skyfoundry.com/doc/docSkySpark/AxonLang#lambdas) should use the blocking structure specified above even when shortcuts
are possible. The parameters must be enclosed within parentheses.

    // Acceptable
    (val) => do
      // do stuff
    end

    // Not acceptable
    val => do
      // do stuff
    end

    // Not acceptable
    (val1, val2) => // do stuff

Like variables, lambdas are not habitually declared at the start of their containing block or block-like construct. Instead, they are
declared close to the point they are first used (within reason).

#### 4.8.4.1 Trailing Lambdas
[Trailing lambdas](https://skyfoundry.com/doc/docSkySpark/AxonLang#trailingLambda) are subject to the same restrictions as ordinary lambdas,
with two exceptions:

1. If the trailing lambda contains only a single statement, the `do`/`end` block may be removed and the statement may be placed on the same
as the function call.
2. If the trailing lambda takes only a single parameter, then the argument parentheses may be excluded.

The trailing lambda **may not** be pulled out of the function call parentheses.

    // Acceptable
    gridVariable.map((val, name) => do
      doSomething()
    end)

    // Acceptable
    gridVariable.each(item => do
      doSomething()
    end)

    // Acceptable
    gridVariable.each(item => doSomething())

    // Not acceptable
    gridVariable.each() item => do
      doSomething()
    end

    // Not acceptable
    gridVariable.each item => do
      doSomething()
    end


#### 4.8.5 Calls

#### 4.8.5.1 Calls are explicit
Parentheses are used on **every** function call, even if the parentheses are empty. This is to provide consistency with the language
requirement that parameter-less funcs require parentheses to differentiate between a symbol or func reference and the evaluated func result.

    val.toStr() // Acceptable
    val.toStr   // Not acceptable

    context() // Acceptable
    context // Language does not support

#### 4.8.5.2 Dot calls are encouraged
The [Dot Call](https://skyfoundry.com/doc/docSkySpark/AxonLang#dotCalls) format is encouraged, as it allows code to more closely match a
programmers' thought sequence. If not used, the programmer should be able to justify why the ordinary form is preferable.

    // Encouraged
    read(point).hisRead(yesterday()).hisRollupAuto()

    // Discouraged
    hisRollupAuto(hisRead(read(point), yesterday()))

#### 4.8.6 Returns
Explicit return keywords (as opposed to implied returns via the last line of a func or lambda) are **strongly preferred**, unless specifically exempted elsewhere in this document. That said, the 'return' keyword can have an
[impact on Axon performance](https://skyfoundry.com/forum/topic/4660) if it is not the last statement. So, on loops where performance is a
concern, the 'return' may be excluded, although we **strongly prefer** that functions with performance requirements be written in Fantom
rather than Axon.

    // Preferred
    (condition) => do
      if (condition) do
        return doSomething(result)
      end else do
        return doSomethingElse(result)
      end
    end

    // Not preferred, and acceptable only under performance considerations
    () => do
      if (condition) do
        doSomething(result)
      end else do
        doSomethingElse(result)
      end
    end

**Exception**: If the block is a trailing lambda and contains only a single statement, then the return may be excluded

    // Acceptable
    listVariable.findAll(item => do
      item == 3
    end)

## 5 Naming

### 5.1 Rules common to all identifiers
Identifiers use only ASCII letters and digits, and, in a small number of cases noted below, underscores. Thus each valid identifier name is
matched by the regular expression `\w+`.

### 5.2 Rules by identifier type

#### 5.2.1 Func names
Func names are written in **lowerCamelCase** and are typically verbs or verb phrases. For example, `sendMessage` or `stop`.

Func names should be given appropriate prefixes to group similar functions when sorted alphabetically. Here are a few examples:

- Funcs in pods must start with the name of the pod (with `Ext` removed): `bfitCore`, `bfitAnalytics`, etc
- BuildingFit-internal debugging funcs should start with `bfit`.
- Funcs that implement a feature should start with a name for that feature: `summaryEmail`, `regression`, etc.
- SkySpark rec funcs that only apply to a given portion of the project should start with a name for that portion: `plant`, `towerA`,
`bacnet`, etc.

#### 5.2.2 Variable names
Variable (and parameter) names are written in **lowerCamelCase**. One-character variable names are highly discouraged.

### 5.3 Camel case defined
Sometimes there is more than one reasonable way to convert an English phrase into camel case, such as when acronyms or unusual constructs
like "IPv6" or "iOS" are present. To improve predictability, we specify the following (nearly) deterministic scheme.

Beginning with the prose form of the name:

1. Convert the phrase to plain ASCII and remove any apostrophes. For example, "Müller's algorithm" might become "Muellers algorithm".
2. Divide this result into words, splitting on spaces and any remaining punctuation (typically hyphens).
    - *Recommended*: if any word already has a conventional camel-case appearance in common usage, split this into its constituent parts
    (e.g., "AdWords" becomes "ad words"). Note that a word such as "iOS" is not really in camel case per se; it defies any convention, so
    this recommendation does not apply.
3. Now lowercase everything (including acronyms), then uppercase only the first character of:
    - ... each word, to yield upper camel case, or
    - ... each word except the first, to yield lower camel case
4. Finally, join all the words into a single identifier.

Note that the casing of the original words is almost entirely disregarded. Examples:

| Prose form | Correct | Incorrect |
|------------|---------|-----------|
| "XML HTTP request" | XmlHttpRequest | XMLHTTPRequest |
| "new customer ID" | newCustomerId | newCustomerID |
| "inner stopwatch" | innerStopwatch | innerStopWatch |
| "supports IPv6 on iOS?" | supportsIpv6OnIos | supportsIPv6OnIOS |
| "YouTube importer" | YouTubeImporter/YoutubeImporter* | |

\*Acceptable, but not recommended.

**Note**: Some words are ambiguously hyphenated in the English language: for example "nonempty" and "non-empty" are both correct, so the
func names `checkNonempty` and `checkNonEmpty` are likewise both correct.

## 6 Programming Practices

### 6.1 Caught exceptions are not ignored
It is very rarely correct to do nothing in response to a caught exception. (Typical responses are to log it, or if it is considered
"impossible", rethrow it.)

When it truly is appropriate to take no action whatsoever in a catch block, the reason should be explained in a comment.

    try do
      val: valStr.parseInt()
      return handleNumericResponse(i)
    end catch (err) do
      null // it's not numeric; that's fine, just continue
    end
    return handleNonNumericResponse(response)
